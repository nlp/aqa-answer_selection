#! /usr/bin/env python3
"""
Scripts which utilizes the answer selection module
for answering user-supplied queries
"""
import sys
import os
from model import BertAnswerSelection
import torch
from sqad_db import SqadDb
from query_database import get_record


device = "cuda"
FULL_PATH = "/nlp/projekty/question_answering/AQA_v2/sqad_tools/bert_AS/bert_as_torch"


class AnswerSelection:
    def __init__(self, trained_model, sqad_url, sqad_port, sqad_path, batch_size=32):
        self.model = BertAnswerSelection(trained_model, tokenizer_path="xlm-roberta-large").to(device).eval()
        self.database = SqadDb(file_name=sqad_path, read_only=True)
        self.batch_size = batch_size

    @staticmethod
    def truncate(sequence, sep_token_id):
        truncate_idx = sequence.index(sep_token_id)
        extra_length = len(sequence) - 256
        question = sequence[:truncate_idx + 1]
        answer = sequence[truncate_idx + 1 + extra_length:]
        return question + answer, truncate_idx, extra_length

    @staticmethod
    def sent_join(sent):
        return " ".join(map(lambda x: x['word'], sent))

    def process_document(self, record):
        model_sentences = []
        for sent in record['text']:
            model_sentences.append([self.sent_join(sent['ctx']['prev_sent_b'][0][0]['sent']), self.sent_join(sent['sent'])])
        return model_sentences

    def answer_selection(self, input_question, ds_document_no):
        results = []
        record = get_record(self.database, ds_document_no, old=False, word_parts='w')
        doc_text = self.process_document(record)
        while doc_text:
            portion = doc_text[:self.batch_size]
            portion_sentences = [' '.join(x) for x in portion]
            doc_text = doc_text[self.batch_size:]
            tokens = self.model.tokenizer([input_question] * len(portion_sentences), portion_sentences,
                                           add_special_tokens=True,
                                           max_length=256,
                                           padding="max_length",
                                           return_attention_mask=True,
                                           truncation=True,
                                           return_tensors='pt')
            predictions = self.model.model.forward(tokens["input_ids"].to(device), tokens["attention_mask"].to(device)).logits
            float_predictions = [float(x) for x in predictions]
            results += list(zip(float_predictions, portion))
        return input_question, sorted(results, key=lambda x: x[0], reverse=True), record['url']


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='AQA answer selection module.')
    parser.add_argument("-m", "--model",
                        help="Specifies model path", required=False,
                        default=os.path.join(FULL_PATH, "scripts/best_model_dir"))
    parser.add_argument("--url", "-u",
                        help="SQAD db URL", required=False,
                        default="0.0.0.0")
    parser.add_argument("--port", "-p",
                        help="SQAD db port", required=False,
                        default=9001)
    parser.add_argument("--db_path",
                        help="SQAD db path", required=False,
                        default="/nlp/projekty/question_answering/AQA_v2/sqad_tools/sqad2database/sqad_db/stable")
    parser.add_argument('-q', '--question', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input question in raw form.')
    parser.add_argument("-d", "--document", required=True, type=str,
                        help="Document number from document selection")
    args = parser.parse_args()

    ans_sel = AnswerSelection(args.model, args.url, args.port, args.db_path)
    question, as_answer = ans_sel.answer_selection(args.question.read(), args.document)
    print(f"Question: {question}")

    print("Selected answer:")
    for score, answer in as_answer:
        print(f'{score:.4f}: {answer}')

